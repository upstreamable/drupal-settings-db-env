<?php

$databases['default']['default'] = [
  'driver' => getenv('DB_CONNECTION'),
  'database' => getenv('MYSQL_DATABASE') ?? getenv('DRUPAL_DATABASE_NAME'),
  'username' => getenv('MYSQL_USER') ?? getenv('DRUPAL_DATABASE_USERNAME'),
  'password' => getenv('MYSQL_PASSWORD') ?? getenv('DRUPAL_DATABASE_PASSWORD'),
  'host' => getenv('MYSQL_HOST') ?? getenv('DRUPAL_DATABASE_HOST'),
  'port' => getenv('MYSQL_PORT') ?? getenv('DRUPAL_DATABASE_PORT'),
];
